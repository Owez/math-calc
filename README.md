# math-calc

`math-calc` or "Arithmetic Calculator" is a simple, text-based arithmetic library that focuses on calculation chaining.

This means you can input multiple arithmetic operations at once and quickly calculate the result. For example, `(10 + 60) / 5, 4 + 20` would result in `[14, 24]`.

## Links

### Documentation

The documentation for this arithmetic calculator can be found **[here](https://docs.rs/math-calc/latest/math-calc/)** on [Doc.rs](https://docs.rs).

### Repository

The source code is hosted on [GitLab](https://gitlab.com) and can be found **[here](https://gitlab.com/Owez/math-calc)**.
